<?php

namespace App\Tests;

use App\Service\MailService;
use PHPUnit\Framework\TestCase;

class MailServiceTest extends TestCase
{
    public function testMailSendTrue()
    {
        $user = $this->getMockBuilder('App\Entity\User')
            ->setMethods(['getAge'])->getMock();
        $user->method("getAge")->will($this->returnValue(18));

        $this->assertTrue(MailService::send($user));
    }

    public function testMailSendFalse()
    {
        $user = $this->getMockBuilder('App\Entity\User')
            ->setMethods(['getAge'])->getMock();
        $user->method("getAge")->will($this->returnValue(8));

        $this->assertFalse(MailService::send($user));
    }
}
