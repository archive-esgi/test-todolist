<?php

namespace App\Entity;

use App\Service\MailService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ToDoListRepository")
 */
class ToDoList
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Item", mappedBy="todolist")
     */
    private $items;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", mappedBy="todolist", cascade={"persist", "remove"})
     */
    private $owner;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): ?self
    {
        if (!$item->isValid() || !$this->canAddItem($item)) {
            return null;
        }

        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setTodolist($this);
        }

        MailService::send($this->getOwner());

        return $this;
    }

    /**
     * @param Item $item
     * @return bool
     */
    private function canAddItem($item): bool
    {
        if (!$item->getCreatedAt()) {
            return false;
        }

        $lastItem = $this->items->last();
        if (!$lastItem) {
            return true;
        }

        $diff = $item->getCreatedAt()->diff($lastItem->getCreatedAt());
        $min = 0;

        if (!$diff) {
            return false;
        }

        $min += $diff->days * 1440;
        $min += $diff->h * 60;
        $min += $diff->m;

        return $min >= 30;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getTodolist() === $this) {
                $item->setTodolist(null);
            }
        }

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        // set (or unset) the owning side of the relation if necessary
        $newTodolist = null === $owner ? null : $this;
        if ($owner->getTodolist() !== $newTodolist) {
            $owner->setTodolist($newTodolist);
        }

        return $this;
    }
}
