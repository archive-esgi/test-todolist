<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{

    public function testIsValidEmailTrue() {
        $user = new User();
        $user->setEmail('test@test.com');
        $this->assertTrue($user->isValidEmail());
    }

    public function testIsValidEmailFalse() {
        $user = new User();
        $user->setEmail('testtest.com');
        $this->assertFalse($user->isValidEmail());
    }

    public function testIsValidPasswordTrue() {
        $user = new User();
        $user->setPassword('testpassword');
        $this->assertTrue($user->isValidPassword());
    }

    public function testIsValidPasswordFalse() {
        $user = new User();
        $user->setPassword('test');
        $this->assertFalse($user->isValidPassword());
    }

    public function testAgeMustTrue() {
        $user = new User();
        $user->setBirthday(new \DateTime("-14 years"));
        $this->assertGreaterThanOrEqual(13, $user->getAge());
    }

    public function testAgeMustFalse() {
        $user = new User();
        $user->setBirthday(new \DateTime("-2 days"));
        $this->assertLessThan(13, $user->getAge());
    }

    public function testIsValidTrue() {
        $user = new User();
        $user->setEmail('test@test.com')
            ->setFirstname('Jon')
            ->setLastname('Doo')
            ->setPassword('testpassword')
            ->setBirthday(new \DateTime("-14 years"));
        $this->assertTrue($user->isValid());
    }

    public function testIsValidFalse() {
        $user = new User();
        $user->setEmail('testtest.com')
            ->setPassword('test')
            ->setBirthday(new \DateTime("-2 days"));
        $this->assertFalse($user->isValid());
    }
}
